use Employees

Create table [Lookup](
	Lookup_ID int identity(1,1) primary key,
	[Value] varchar(100) not null,
	Category varchar(50) not null
)

Create table Person(
	PersonID int IDENTITY(1,1) primary key,
	Per_FName varchar(50) not null,
	Per_LName varchar(50) not null,
	Per_Email varchar(100),
	Per_Phone varchar(20),
	Per_Status int not null,
	Per_Gender int not null,
	Foreign key ([Per_Status]) References [Lookup](Lookup_ID),
	Foreign key (Per_Gender) References [Lookup](Lookup_ID)
)


Create table Jobs(
	JobID int IDENTITY(1,1) primary key,
	JobTitle varchar(100),
	Salary Decimal(10,2),
	[Status] int,
	Foreign key ([Status]) References [Lookup](Lookup_ID)
)


Create table Employee(
	EmployeeID int Primary Key,
	JobID int,
	Emp_Hiredate datetime not null, 
	ManagerID int,
	Foreign Key (JobID) References Jobs(JobID),
	Foreign Key (ManagerID) References Employee(EmployeeID),
	Foreign Key (EmployeeID) References Person(PersonID)
)


Create table PerformanceMetrics(
	MetricID int identity(1,1) Primary key,
	JobID int not null,
	MetricName varchar(255) not null,
	Metric_Weightage decimal (5,2) not null,
	Metric_TotalScore decimal(5,2) not null,
	Description Text,
	Foreign key (JobId) References Jobs(JobId)
)


CREATE TABLE Emp_Performance (
    EmployeeID INT,
    MetricID INT,
    ObtainedScore DECIMAL(5,2) NOT NULL,
    DateRecorded DATETIME,
    PRIMARY KEY (EmployeeID, MetricID),
    FOREIGN KEY (EmployeeID) REFERENCES Employee(EmployeeID),
    FOREIGN KEY (MetricID) REFERENCES PerformanceMetrics(MetricID)
);

CREATE TABLE Attendance (
    AttendanceID INT IDENTITY(1,1) PRIMARY KEY,
    EmployeeID INT NOT NULL,
    AttendanceDate DATE NOT NULL,
    AttendanceStatus INT NOT NULL,
    FOREIGN KEY (EmployeeID) REFERENCES Employee(EmployeeID)
);

Create table TopPerformers(
	TopPerformerID int identity(1,1) primary key,
	EmployeeID int not null,
	PerformanceScore decimal(5,2),
	DateAcheived datetime,
	Foreign Key (EmployeeID) References Employee(EmployeeID)
)
