use Employees
-- Trigger when a job is assign
CREATE TRIGGER trg_AssignJob_Limit
ON Employee
AFTER INSERT
AS
BEGIN
    DECLARE @JobID INT
    DECLARE @CurrentAssignments INT

    SELECT @JobID = JobID FROM inserted

    SELECT @CurrentAssignments = COUNT(*)
    FROM Employee
    WHERE JobID = @JobID

    IF @CurrentAssignments > 5
    BEGIN
        RAISERROR ('Cannot assign more than 5 employees to this job.', 16, 1)
        ROLLBACK TRANSACTION
    END
END;

-- Trigger when a metrics is added
CREATE TRIGGER trg_PerformanceMetrics_MaxMetrics
ON PerformanceMetrics
AFTER INSERT
AS
BEGIN
    DECLARE @JobID INT
    DECLARE @MetricsCount INT

    SELECT @JobID = JobID FROM inserted

    SELECT @MetricsCount = COUNT(*)
    FROM PerformanceMetrics
    WHERE JobID = @JobID

    IF @MetricsCount > 5
    BEGIN
        RAISERROR ('Cannot add more than 5 metrics to a job.', 16, 1)
        ROLLBACK TRANSACTION
    END
END;


-- Trigger when a job is deleted
CREATE TRIGGER trg_Jobs_CheckMetrics
ON Jobs
AFTER DELETE
AS
BEGIN
    DECLARE @DeletedJobID INT
    DECLARE @MetricsCount INT

    SELECT @DeletedJobID = JobID FROM deleted

    SELECT @MetricsCount = COUNT(*)
    FROM PerformanceMetrics
    WHERE JobID = @DeletedJobID

    IF @MetricsCount > 5
    BEGIN
        RAISERROR ('Cannot delete a job with more than 5 metrics.', 16, 1)
        ROLLBACK TRANSACTION
    END
END;