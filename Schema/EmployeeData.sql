use Employees

-- Inserting into lookup table
Insert [Lookup] ([Value],Category) values (N'Active',N'Status')
Insert [Lookup] ([Value],Category) values (N'Inactive',N'Status')
Insert [Lookup] ([Value],Category) values (N'Male',N'Gender')
Insert [Lookup] ([Value],Category) values (N'Female',N'Gender')
Insert [Lookup] ([Value],Category) values (N'Present',N'Attendence State')
Insert [Lookup] ([Value],Category) values (N'Absent',N'Attendence State')
Insert [Lookup] ([Value],Category) values (N'Late',N'Attendence State')
Insert [Lookup] ([Value],Category) values (N'Leave',N'Attendence State')

-- Inserting into jobs
INSERT INTO Jobs (JobTitle, Salary,[Status]) VALUES 
    ('Software Engineer', CAST(85000.00 AS Decimal(8, 2)),1),
    ('Quality Assurance Engineer', CAST(75000.00 AS Decimal(8, 2)),1),
    ('DevOps Engineer', CAST(90000.00 AS Decimal(8, 2)),1),
    ('Systems Analyst', CAST(80000.00 AS Decimal(8, 2)),1),
    ('Database Developer', CAST(85000.00 AS Decimal(8, 2)),1),
    ('Network Administrator', CAST(80000.00 AS Decimal(8, 2)),1),
    ('UI/UX Designer', CAST(80000.00 AS Decimal(8, 2)),1),
    ('Data Analyst', CAST(85000.00 AS Decimal(8, 2)),1),
    ('IT Support Specialist', CAST(70000.00 AS Decimal(8, 2)),1),
    ('Scrum Master', CAST(90000.00 AS Decimal(8, 2)),1),
    ('Release Manager', CAST(95000.00 AS Decimal(8, 2)),1),
    ('Technical Architect', CAST(100000.00 AS Decimal(8, 2)),1),
    ('Information Security Analyst', CAST(95000.00 AS Decimal(8, 2)),1),
    ('Cloud Engineer', CAST(90000.00 AS Decimal(8, 2)),1),
    ('Business Intelligence Developer', CAST(85000.00 AS Decimal(8, 2)),1),
    ('AI/ML Engineer', CAST(110000.00 AS Decimal(8, 2)),1),
    ('Software Development Manager', CAST(120000.00 AS Decimal(8, 2)),1),
    ('Product Owner', CAST(100000.00 AS Decimal(8, 2)),1),
    ('Release Engineer', CAST(90000.00 AS Decimal(8, 2)),1),
    ('Data Scientist', CAST(105000.00 AS Decimal(8, 2)),1);

