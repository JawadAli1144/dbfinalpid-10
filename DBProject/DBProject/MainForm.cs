﻿using DBProject.UserForms;
using DBProject.UserForms.Attendance;
using DBProject.UserForms.Employee;
using DBProject.UserForms.Job;
using DBProject.UserForms.PerformanceMatrics;
using DBProject.UserForms.Reports;
using DBProject.UserForms.TopPerformers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        

        private void addPanel(UserControl usercontrol)
        {
            usercontrol.Dock = DockStyle.Fill;
            MainPanel.Controls.Clear();
            MainPanel.Controls.Add(usercontrol);
            usercontrol.BringToFront();
        }

        private void btnEmployee_Click(object sender, EventArgs e)
        {
            ViewAllEmployeeForm viewEmp = new ViewAllEmployeeForm();
            addPanel(viewEmp);
        }

        private void btnCustomer_Click(object sender, EventArgs e)
        {
           
        }

        private void btnEvaluation_Click(object sender, EventArgs e)
        {
            EvaluationMenuForm evform = new EvaluationMenuForm();
            addPanel(evform);
        }

        private void btnTopPerformers_Click(object sender, EventArgs e)
        {
            TopPerformersForm topPerformer = new TopPerformersForm();
            addPanel(topPerformer);
        }

        private void btnPerformanceMatric_Click(object sender, EventArgs e)
        {
            ViewPerformanceMatricForm viewperformancematric = new ViewPerformanceMatricForm();
            addPanel(viewperformancematric);
        }

        private void btnAttendance_Click(object sender, EventArgs e)
        {
            ViewAttendanceRecord attendance = new ViewAttendanceRecord();
            addPanel(attendance);
        }

        private void btnReports_Click(object sender, EventArgs e)
        {
            ReportsMenu rpt = new ReportsMenu();
            addPanel(rpt);
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            // addd job form
            ViewJobForm viewJob = new ViewJobForm();
            addPanel(viewJob);
        }
    }
}
