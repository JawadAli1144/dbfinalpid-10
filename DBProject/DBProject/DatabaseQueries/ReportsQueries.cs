﻿using MidProject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace DBProject.DatabaseQueries
{
    internal class ReportsQueries
    {

        public static void LoadAttendanceDataToCSV(string csvFilePath)
        {
            var con = Configuration.getInstance().getConnection();
            string query = @"
    SELECT 
        P.Per_FName,
        P.Per_LName,
        J.JobTitle,
        A.AttendanceDate,
        CASE A.AttendanceStatus
            WHEN 5 THEN 'Present'
            WHEN 6 THEN 'Absent'
            WHEN 7 THEN 'Late'
            WHEN 8 THEN 'Leave'
            ELSE 'Unknown'
        END AS AttendanceStatus    
    FROM 
        Attendance A
    LEFT JOIN 
        Employee E ON E.EmployeeID = A.EmployeeID
    LEFT JOIN 
        Jobs J ON E.JobID = J.JobID
    LEFT JOIN 
        Person P ON E.EmployeeID = P.PersonID
";


            using (SqlCommand command = new SqlCommand(query, con))
            using (SqlDataReader reader = command.ExecuteReader())
            using (StreamWriter writer = new StreamWriter(csvFilePath))
            {
                writer.WriteLine("FirstName,LastName,JobTitle,AttendanceDate,AttendanceStatus");

                while (reader.Read())
                {
                    string firstname = reader["Per_FName"].ToString();
                    string lastname = reader["Per_LName"].ToString();
                    string job = reader["JobTitle"].ToString();
                    string date = ((DateTime)reader["AttendanceDate"]).ToString("yyyy-MM-dd");
                    string status = reader["AttendanceStatus"].ToString();

                    writer.WriteLine($"{firstname},{lastname},{job},{date},{status}");
                }
            }

        }

        public static DataTable ViewAllEmployees()
        {
            DataTable Employee = new DataTable();
            Employee.Columns.Add("Person ID");
            Employee.Columns.Add("Employee Name");
            //Employee.Columns.Add("Job Title");
            Employee.Columns.Add("Attendence Month");
            Employee.Columns.Add("Present Count");
            Employee.Columns.Add("Late Count");
            Employee.Columns.Add("Leave Count");
            Employee.Columns.Add("Absent Count");
            string q = "SELECT e.EmployeeID, CONCAT(p.Per_FName, ' ' ,p.Per_LName) AS EmployeeName, MONTH(a.AttendanceDate) AS AttendanceMonth, SUM(CASE WHEN a.AttendanceStatus = 5 THEN 1 ELSE 0 END) AS PresentCount, SUM(CASE WHEN a.AttendanceStatus = 7 THEN 1 ELSE 0 END) AS LateCount, SUM(CASE WHEN a.AttendanceStatus = 8 THEN 1 ELSE 0 END) AS LeaveCount, SUM(CASE WHEN a.AttendanceStatus = 6 THEN 1 ELSE 0 END) AS AbsentCount FROM Employee e JOIN Person p ON e.EmployeeID = p.PersonID LEFT JOIN Attendance a ON e.EmployeeID = a.EmployeeID GROUP BY e.EmployeeID, p.Per_FName, p.Per_LName, MONTH(a.AttendanceDate) ORDER BY e.EmployeeID, AttendanceMonth";
            SqlCommand sqlcmd1 = new SqlCommand(q, Configuration.getInstance().getConnection());
            SqlDataReader dr = sqlcmd1.ExecuteReader();
            while (dr.Read())
            {
                Employee.Rows.Add(dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), dr[5].ToString(), dr[6].ToString());
            }

            return Employee;
        }

        public static void CreateWordDocumentFromDataTable(string title, DataTable dataTable)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Word Document (.docx)|*.docx";
            saveFileDialog.Title = "Export to Word";
            saveFileDialog.FileName = title;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                // Create Word application object
                Word.Application wordApp = new Word.Application();
                wordApp.Visible = true; // Set to true to make Word application visible

                // Create a new document
                Word.Document doc = wordApp.Documents.Add();

                // Add content to the document
                Word.Paragraph heading1 = doc.Content.Paragraphs.Add();
                heading1.Range.Text = "University of Engineering & Technology, Lahore";
                heading1.Range.Font.Bold = 1;
                heading1.Format.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;

                Word.Paragraph heading2 = doc.Content.Paragraphs.Add();
                heading2.Range.Text = "Department of Computer Sciences";
                heading2.Range.Font.Bold = 1;
                heading2.Format.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;

                Word.Paragraph heading = doc.Content.Paragraphs.Add();
                heading.Range.Text = title;
                heading.Range.Font.Bold = 1;
                heading.Format.Alignment = Word.WdParagraphAlignment.wdAlignParagraphCenter;

                // Add table to the document
                Word.Table table = doc.Tables.Add(doc.Range(), dataTable.Rows.Count + 1, dataTable.Columns.Count);
                foreach (DataColumn column in dataTable.Columns)
                {
                    table.Cell(1, column.Ordinal + 1).Range.Text = column.ColumnName;
                }

                // Populate table with data
                for (int row = 0; row < dataTable.Rows.Count; row++)
                {
                    for (int col = 0; col < dataTable.Columns.Count; col++)
                    {
                        table.Cell(row + 2, col + 1).Range.Text = dataTable.Rows[row][col].ToString();
                    }
                }

                // Save the document
                object fileName = saveFileDialog.FileName;
                doc.SaveAs2(fileName);

                // Close Word application
                // wordApp.Quit();
                MessageBox.Show("File saved");
            }
        }

        public static void ExportTopPerformersToCSV()
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV files (*.csv)|*.csv";
            saveFileDialog.Title = "Export Top Performers to CSV";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                string filePath = saveFileDialog.FileName;
                using (StreamWriter writer = new StreamWriter(filePath))
                {
                    
                    SqlCommand cmd = TopPerformerQueries.ViewTopPerformers1();
                    var con = Configuration.getInstance().getConnection();
                    cmd.Connection = con;
                    SqlDataReader dr = cmd.ExecuteReader();
                    writer.WriteLine("EmployeeID,PerformanceScore,DateAcheived");

                    while (dr.Read())
                    {
                        string line = $"{dr["EmployeeID"]},{dr["PerformanceScore"]}, {dr["DateAcheived"]}";
                        writer.WriteLine(line);
                    }
                    dr.Close();
                }

                MessageBox.Show("Top performers data has been exported to CSV file successfully.", "Export Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

    }

}
