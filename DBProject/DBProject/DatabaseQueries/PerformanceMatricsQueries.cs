﻿using MidProject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace DBProject.DatabaseQueries
{
    internal class PerformanceMatricsQueries
    {
        public static void InsertIntoPerformanceMetricTable(string JobID, string MetricName, string Weightage, string TotalScore, string desc)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_InsertIntoPerformanceMetrics", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@JobID", JobID);
            cmd.Parameters.AddWithValue("@MetricName", MetricName);
            cmd.Parameters.AddWithValue("@Weightage", Weightage);
            cmd.Parameters.AddWithValue("@TotalScore", TotalScore);
            cmd.Parameters.AddWithValue("@Description", desc);

            cmd.ExecuteNonQuery();
        }



        public static SqlCommand ViewAllPerfomanceMatric()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * From View_ViewPerformanceMetric", con);
            return cmd;
        }

        public static SqlCommand ViewAllPerfomanceMatricOfSpecificJob(string jobName)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_ViewAllPerfomanceMatricOfSpecificJob", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@jobname", jobName);
            return cmd;
        }

        public static SqlCommand GetAllJobs()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select JobTitle From Jobs", con);
            return cmd;
        }

        public static SqlCommand GetJobByName(string JobName)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_GetJobByName", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@JobName", JobName);
            return cmd;
        }

        public static string GetMetricScoreByID(string MetricID)
        {
            string score = "";
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select TotalScore From PerformanceMetrics where MetricID = @metricId", con);
            cmd.Parameters.AddWithValue("@metricId", MetricID);
            SqlDataReader reader = cmd.ExecuteReader();
            if(reader.Read())
            {
                score = reader.GetString(0);
            }
            reader.Close();
            return score;
        }


    }
}
