﻿using Microsoft.Office.Interop.Word;
using MidProject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.WebSockets;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace DBProject.DatabaseQueries
{
    internal class EmployeeQueries
    {
        public static SqlCommand GetAllEmployees()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * From View_ViewAllEmployees", con);
            return cmd;
        }

        public static int GetGenderID(string gender)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select Lookup_ID from Lookup where Value = @Gender", con);
                cmd.Parameters.AddWithValue("@Gender", gender);
                int id = int.Parse(cmd.ExecuteScalar().ToString());
                return id;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }

        public static int GetJobID(string JobName)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select JobID  from Jobs where JobTitle = @JobName", con);
                cmd.Parameters.AddWithValue("@JobName", JobName);
                int id = int.Parse(cmd.ExecuteScalar().ToString());
                return id;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }

        public static int GetManagerID(string firstname, string lastname)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select PersonID from Person where Per_FName = @firstname and Per_LName = @lastname", con);
                cmd.Parameters.AddWithValue("@firstname", firstname);
                cmd.Parameters.AddWithValue("@lastname", lastname);
                int id = int.Parse(cmd.ExecuteScalar().ToString());
                return id;
            }
            catch (SqlException ex)
            {
                return -1;
            }
        }

        public static void InsertIntoPersonTable(string firstname, string lastname, string email, string phone,string status, string gender)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_InsertIntoPerson", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@FirstName", firstname);
            cmd.Parameters.AddWithValue("@LastName", lastname);
            cmd.Parameters.AddWithValue("@Email", email);
            cmd.Parameters.AddWithValue("@Phone", phone);
            cmd.Parameters.AddWithValue("@Status", status);
            cmd.Parameters.AddWithValue("@Gender", gender);

            cmd.ExecuteNonQuery();
        }

        public static int SelectMaxFromPersonTable()
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select Max(PersonID) From Person", con);
                int ans = Convert.ToInt32(cmd.ExecuteScalar());
                return ans;
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return -1;
            }
        }

        public static void InsertIntoEmployeeTable(string employeeID, string jobID, DateTime Hiredate, string ManagerID)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_InsertIntoEmployee", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@EmployeeID", employeeID);
            cmd.Parameters.AddWithValue("@JobID", jobID);
            cmd.Parameters.AddWithValue("@HireDate", Hiredate);
            cmd.Parameters.AddWithValue("@ManagerID", ManagerID);
            cmd.ExecuteNonQuery();
        }

        public static SqlCommand GetAllEmployeesByJob(string jobName)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("sp_GetAllEmployeesByJob", con);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.Parameters.AddWithValue("@JobName", jobName);
            return cmd1;
        }

        public static SqlCommand GetEmployeeByName(string name)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("sp_GetEmployeeByName", con);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.Parameters.AddWithValue("@name", name);
            return cmd1;
        }

        public static SqlCommand GetEmployeeByDepartment(string jobname)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_GetEmployeeByJob", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@jobname", jobname);
            return cmd;
        }

        public static SqlCommand GetScoreOfEmployee(string EmployeeID)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_GetScoreOfEmployeeByEmployeeID", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            return cmd;
        }

        public static void AddInEmp_Performance(string EmployeeID, string MetricID, string ObtainedMarks, DateTime Date)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Emp_Performance values (@EmployeeID,@MetricID,@ObtainedScore,@DateRecorded)", con);
            cmd.Parameters.AddWithValue("@EmployeeID", EmployeeID);
            cmd.Parameters.AddWithValue("@MetricID", MetricID);
            cmd.Parameters.AddWithValue("@ObtainedScore", ObtainedMarks);
            cmd.Parameters.AddWithValue("@DateRecorded", Date);
            cmd.ExecuteNonQuery();
        }

        public static int CheckTheMetricIDInEmp_Performance(string EmpID, string MetricID) 
        {
            int count = -1;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select count(*) from Emp_Performance where EmployeeID = @EmpID and MetricID = @metricID", con);
            cmd.Parameters.AddWithValue("@EmpID", EmpID);
            cmd.Parameters.AddWithValue("@metricID", MetricID);
            count = (int)cmd.ExecuteScalar();
            return count;
        }

        public static SqlCommand GetScoreOfAllEmployees()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Select * From View_GetScoreOfAllEmployees", con);
            return cmd1;
        }

        public static SqlCommand GetScoreOfEmployeesbyJob(string JobName)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("sp_GetScoreOfEmployeeByJob", con);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.Parameters.AddWithValue("@JobName", JobName);
            return cmd1;
        }

        public static SqlCommand GetScoreOfEmployeesbyName(string Name)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("sp_GetScoreOfEmployeeByName", con);
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.Parameters.AddWithValue("@name", Name);
            return cmd1;
        }

        public static void DeleteEmployee(string id)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete From TopPerformers where EmployeeID = @id", con);
            cmd.Parameters.AddWithValue("@id", id);
            cmd.ExecuteNonQuery();

            SqlCommand cmd1 = new SqlCommand("Delete From Attendance where EmployeeID = @id",con);
            cmd1.Parameters.AddWithValue("@id", id);
            cmd1.ExecuteNonQuery();

            SqlCommand cmd2 = new SqlCommand("Delete From Emp_Performance where EmployeeID = @id",con);
            cmd2.Parameters.AddWithValue("@id", id);
            cmd2.ExecuteNonQuery();

            SqlCommand cmd3 = new SqlCommand("Update Person set Per_Status = @status where PersonID = @id", con);
            cmd3.Parameters.AddWithValue("@id", id);
            cmd3.Parameters.AddWithValue("@status", 2);
            cmd3.ExecuteNonQuery();

            

        }
    }
}
