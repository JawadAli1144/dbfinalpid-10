﻿using MidProject;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace DBProject.DatabaseQueries
{
    internal class JobQueries
    {
        public static void AddIntoJob(string JobTitle, string JobSalary, string status)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_InsertIntoJobs", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@JobName", JobTitle);
            cmd.Parameters.AddWithValue("@Salary", JobSalary);
            cmd.Parameters.AddWithValue("@status", status);

            cmd.ExecuteNonQuery();
        }

        public static SqlCommand ViewJobs()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * From View_ViewJobs", con);
            return cmd;
        }

        public static string GetJobName(string JobID)
        {
                string jobTitle = null;
            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("select JobTitle from Jobs where JobID = @JobID", con);
                cmd.Parameters.AddWithValue("@JobID", JobID);
                object result = cmd.ExecuteScalar();
                if (result != null)
                {
                    jobTitle = result.ToString();
                }
                return jobTitle;
            }
            catch (SqlException ex)
            {
                return null;
            }
        }

        public static void DeleteJob(string jobid)
        {
            var con = Configuration.getInstance().getConnection();
            List<string> metricIDs = GetMetricIDsForJob(jobid);
            DeleteMetricIDs(metricIDs);
            SqlCommand cmd = new SqlCommand("Delete from PerformanceMetrics where JobID = @id", con);
            cmd.Parameters.AddWithValue("@id", jobid);
            cmd.ExecuteNonQuery();

            SqlCommand cmd1 = new SqlCommand("update Jobs set Status = @status", con);
            cmd1.Parameters.AddWithValue("@status", 2);
            cmd1.ExecuteNonQuery();

            List<string> EmpID = GetEmployeeIDForEmployee(jobid);
            for(int i=0; i<EmpID.Count; i++)
            {
                EmployeeQueries.DeleteEmployee(i.ToString());
            }

        }


        static List<string> GetEmployeeIDForEmployee(string JobID)
        {
            List<string> EmpID = new List<string>();

            try
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT EmployeeID FROM Employee WHERE JobID = @JobID", con);
                cmd.Parameters.AddWithValue("@JobID", JobID);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    EmpID.Add(reader["EmployeeID"].ToString());
                }

                reader.Close();
            }
            catch (SqlException ex)
            {
                // Handle exceptions here
            }


            return EmpID;
        }

        static List<string> GetMetricIDsForJob(string jobID)
        {
            List<string> metricIDs = new List<string>();

            
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("SELECT MetricID FROM PerformanceMetrics WHERE JobID = @JobID", con);
                cmd.Parameters.AddWithValue("@JobID", jobID);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    metricIDs.Add(reader["MetricID"].ToString());
                }

                reader.Close();
            
            
           
            return metricIDs;
        }

        static void DeleteMetricIDs(List<string> metricIDs)
        {
            try
            {
                var con = Configuration.getInstance().getConnection();
               /* string parameterList = string.Join(",", metricIDs.Select((s, i) => $"@MetricID{i}"));
                string query = $"DELETE FROM Emp_Performance WHERE MetricID IN ({parameterList})";

                SqlCommand cmd = new SqlCommand(query, con);
                for (int i = 0; i < metricIDs.Count; i++)
                {
                    cmd.Parameters.AddWithValue($"@MetricID{i}", metricIDs[i]);
                }
                cmd.ExecuteNonQuery();*/

                for(int i=0; i<metricIDs.Count; i++)
                {
                    SqlCommand cmd = new SqlCommand("Delete from Emp_Performance where metricID = @metricid", con);
                    cmd.Parameters.AddWithValue("@metricid", i);
                    cmd.ExecuteNonQuery();
                }

            }
            catch (SqlException ex)
            {
                // Handle exceptions here
            }
            
        }

    }
}
