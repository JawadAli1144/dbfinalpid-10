﻿using MidProject;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBProject.DatabaseQueries
{
    internal class AttendanceQueries
    {
        public static SqlCommand ViewEmployees()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from View_ViewEmployeeInAttendanceMenu", con);
            return cmd;
        }

        public static SqlCommand ViewEmployees(string EmpName)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_GetEmployeeByJobNameInAttendanceMenu ", con);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@name", EmpName);
            return cmd;
        }

        public static SqlCommand ViewAttendance(string EmployeeID)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_ViewAttendanceByEmployeeID", con);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@empID", EmployeeID);
            return cmd;
        }

        


    }
}
