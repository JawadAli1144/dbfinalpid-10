﻿using MidProject;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.WebSockets;

namespace DBProject.DatabaseQueries
{
    internal class TopPerformerQueries
    {
        public static SqlCommand InsertIntoTopPerformerTable()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand countcmd = new SqlCommand("Select count(*) from TopPerformers",con);
            int count = (int)countcmd.ExecuteScalar();
            if(count != 0)
            {
                SqlCommand del = new SqlCommand("delete from TopPerformers", con);
                del.ExecuteNonQuery();

            }
            SqlCommand cmd2 = new SqlCommand("sp_InsertIntoTopPerformerTable", con);
            
            return cmd2;
        }

        public static SqlCommand ViewTopPerformers()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * From TopPerformers", con);
            return cmd;
        }

        public static SqlCommand ViewTopPerformers1()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select TopPerformers.EmployeeID, TopPerformers.PerformanceScore, TopPerformers.DateAcheived From TopPerformers inner join Employee on Employee.EmployeeID = TopPerformers.EmployeeID inner join Jobs on Jobs.JobID = Employee.JobID inner join Person on Person.PersonID = Employee.EmployeeID", con);
            return cmd;
        }

        public static SqlCommand ViewEmployeeOfSpecificDept(string jobName)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("sp_ViewTopPerformerOfSpecificjob", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@jobName", jobName);
            return cmd;
        }
    }
}
