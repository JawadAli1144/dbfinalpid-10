﻿using DBProject.DatabaseQueries;
using MidProject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject.UserForms.Reports
{
    public partial class ReportsMenu : UserControl
    {
        public ReportsMenu()
        {
            InitializeComponent();
        }

        private void btnViewEvaluations_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV files (*.csv)|*.csv";
            saveFileDialog.Title = "Save Attendance Record";
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            DialogResult result = saveFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                string filePath = saveFileDialog.FileName;
                var con = Configuration.getInstance().getConnection();
                ReportsQueries.LoadAttendanceDataToCSV(filePath);
                MessageBox.Show("Attendence Record have been saved Successfully", "Record Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        private void btnSaveAttendenceReport_Click(object sender, EventArgs e)
        {
            //ReportsQueries.CreateWordDocumentFromDataTable("Attendence Record", ReportsQueries.ViewAllEmployees());
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "CSV files (*.csv)|*.csv";
                saveFileDialog.Title = "Save CSV file";

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    using (StreamWriter writer = new StreamWriter(saveFileDialog.FileName))
                    {
                        writer.WriteLine("Person ID,Attendance Month,Present Count,Late Count,Leave Count,Absent Count");

                        string q = "SELECT e.EmployeeID, MONTH(a.AttendanceDate) AS AttendanceMonth, SUM(CASE WHEN a.AttendanceStatus = 5 THEN 1 ELSE 0 END) AS PresentCount, SUM(CASE WHEN a.AttendanceStatus = 7 THEN 1 ELSE 0 END) AS LateCount, SUM(CASE WHEN a.AttendanceStatus = 8 THEN 1 ELSE 0 END) AS LeaveCount, SUM(CASE WHEN a.AttendanceStatus = 6 THEN 1 ELSE 0 END) AS AbsentCount FROM Employee e JOIN Person p ON e.EmployeeID = p.PersonID LEFT JOIN Attendance a ON e.EmployeeID = a.EmployeeID GROUP BY e.EmployeeID, MONTH(a.AttendanceDate) ORDER BY e.EmployeeID, AttendanceMonth";
                        SqlCommand sqlcmd1 = new SqlCommand(q, Configuration.getInstance().getConnection());
                        SqlDataReader dr = sqlcmd1.ExecuteReader();

                        while (dr.Read())
                        {
                            string line = $"{dr[0]},{dr[1]},{dr[2]},{dr[3]},{dr[4]},{dr[5]}";
                            writer.WriteLine(line);
                        }
                        dr.Close();
                    }

                    MessageBox.Show("CSV file has been saved successfully.", "File Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            
        }

        private void btnSavePerformanceScore_Click(object sender, EventArgs e)
        {
            // performance score csv....

            ReportsQueries.ExportTopPerformersToCSV();
        }
    }
}
