﻿namespace DBProject.UserForms.Reports
{
    partial class ReportsMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainPanel = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.btnSaveAttendenceRecord = new System.Windows.Forms.GroupBox();
            this.btnSaveAttendenceReport = new Guna.UI2.WinForms.Guna2Button();
            this.btnViewEvaluations = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSavePerformanceScore = new Guna.UI2.WinForms.Guna2Button();
            this.MainPanel.SuspendLayout();
            this.btnSaveAttendenceRecord.SuspendLayout();
            this.guna2Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainPanel
            // 
            this.MainPanel.Controls.Add(this.btnSaveAttendenceRecord);
            this.MainPanel.Controls.Add(this.guna2Panel1);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.FillColor = System.Drawing.Color.DodgerBlue;
            this.MainPanel.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.MainPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(1141, 679);
            this.MainPanel.TabIndex = 6;
            // 
            // btnSaveAttendenceRecord
            // 
            this.btnSaveAttendenceRecord.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnSaveAttendenceRecord.BackColor = System.Drawing.Color.Transparent;
            this.btnSaveAttendenceRecord.Controls.Add(this.btnSavePerformanceScore);
            this.btnSaveAttendenceRecord.Controls.Add(this.btnSaveAttendenceReport);
            this.btnSaveAttendenceRecord.Controls.Add(this.btnViewEvaluations);
            this.btnSaveAttendenceRecord.Location = new System.Drawing.Point(321, 137);
            this.btnSaveAttendenceRecord.Name = "btnSaveAttendenceRecord";
            this.btnSaveAttendenceRecord.Size = new System.Drawing.Size(556, 399);
            this.btnSaveAttendenceRecord.TabIndex = 1;
            this.btnSaveAttendenceRecord.TabStop = false;
            // 
            // btnSaveAttendenceReport
            // 
            this.btnSaveAttendenceReport.BorderRadius = 5;
            this.btnSaveAttendenceReport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSaveAttendenceReport.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnSaveAttendenceReport.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnSaveAttendenceReport.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnSaveAttendenceReport.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnSaveAttendenceReport.FillColor = System.Drawing.Color.Navy;
            this.btnSaveAttendenceReport.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnSaveAttendenceReport.ForeColor = System.Drawing.Color.White;
            this.btnSaveAttendenceReport.HoverState.FillColor = System.Drawing.Color.RoyalBlue;
            this.btnSaveAttendenceReport.Location = new System.Drawing.Point(311, 62);
            this.btnSaveAttendenceReport.Name = "btnSaveAttendenceReport";
            this.btnSaveAttendenceReport.Size = new System.Drawing.Size(181, 81);
            this.btnSaveAttendenceReport.TabIndex = 1;
            this.btnSaveAttendenceReport.Text = "Save Attendence Report";
            this.btnSaveAttendenceReport.Click += new System.EventHandler(this.btnSaveAttendenceReport_Click);
            // 
            // btnViewEvaluations
            // 
            this.btnViewEvaluations.BorderRadius = 5;
            this.btnViewEvaluations.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewEvaluations.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnViewEvaluations.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnViewEvaluations.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnViewEvaluations.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnViewEvaluations.FillColor = System.Drawing.Color.Navy;
            this.btnViewEvaluations.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnViewEvaluations.ForeColor = System.Drawing.Color.White;
            this.btnViewEvaluations.HoverState.FillColor = System.Drawing.Color.RoyalBlue;
            this.btnViewEvaluations.Location = new System.Drawing.Point(75, 62);
            this.btnViewEvaluations.Name = "btnViewEvaluations";
            this.btnViewEvaluations.Size = new System.Drawing.Size(181, 81);
            this.btnViewEvaluations.TabIndex = 0;
            this.btnViewEvaluations.Text = "Save Attendence Record";
            this.btnViewEvaluations.Click += new System.EventHandler(this.btnViewEvaluations_Click);
            // 
            // guna2Panel1
            // 
            this.guna2Panel1.BackColor = System.Drawing.Color.DodgerBlue;
            this.guna2Panel1.Controls.Add(this.label1);
            this.guna2Panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.guna2Panel1.Location = new System.Drawing.Point(0, 0);
            this.guna2Panel1.Name = "guna2Panel1";
            this.guna2Panel1.Size = new System.Drawing.Size(1141, 100);
            this.guna2Panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(396, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(335, 67);
            this.label1.TabIndex = 0;
            this.label1.Text = "Save Reports";
            // 
            // btnSavePerformanceScore
            // 
            this.btnSavePerformanceScore.BorderRadius = 5;
            this.btnSavePerformanceScore.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSavePerformanceScore.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnSavePerformanceScore.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnSavePerformanceScore.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnSavePerformanceScore.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnSavePerformanceScore.FillColor = System.Drawing.Color.Navy;
            this.btnSavePerformanceScore.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnSavePerformanceScore.ForeColor = System.Drawing.Color.White;
            this.btnSavePerformanceScore.HoverState.FillColor = System.Drawing.Color.RoyalBlue;
            this.btnSavePerformanceScore.Location = new System.Drawing.Point(87, 186);
            this.btnSavePerformanceScore.Name = "btnSavePerformanceScore";
            this.btnSavePerformanceScore.Size = new System.Drawing.Size(181, 81);
            this.btnSavePerformanceScore.TabIndex = 2;
            this.btnSavePerformanceScore.Text = "Save Performance Score";
            this.btnSavePerformanceScore.Click += new System.EventHandler(this.btnSavePerformanceScore_Click);
            // 
            // ReportsMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MainPanel);
            this.Name = "ReportsMenu";
            this.Size = new System.Drawing.Size(1141, 679);
            this.MainPanel.ResumeLayout(false);
            this.btnSaveAttendenceRecord.ResumeLayout(false);
            this.guna2Panel1.ResumeLayout(false);
            this.guna2Panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2GradientPanel MainPanel;
        private System.Windows.Forms.GroupBox btnSaveAttendenceRecord;
        private Guna.UI2.WinForms.Guna2Button btnViewEvaluations;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2Button btnSaveAttendenceReport;
        private Guna.UI2.WinForms.Guna2Button btnSavePerformanceScore;
    }
}
