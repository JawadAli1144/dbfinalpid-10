﻿namespace DBProject.UserForms
{
    partial class EvaluationMenuForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MainPanel = new Guna.UI2.WinForms.Guna2GradientPanel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnMarkEvaluation = new Guna.UI2.WinForms.Guna2Button();
            this.btnViewEvaluations = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.MainPanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.guna2Panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainPanel
            // 
            this.MainPanel.Controls.Add(this.groupBox1);
            this.MainPanel.Controls.Add(this.guna2Panel1);
            this.MainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MainPanel.FillColor = System.Drawing.Color.DodgerBlue;
            this.MainPanel.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.MainPanel.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical;
            this.MainPanel.Location = new System.Drawing.Point(0, 0);
            this.MainPanel.Name = "MainPanel";
            this.MainPanel.Size = new System.Drawing.Size(1141, 872);
            this.MainPanel.TabIndex = 5;
            this.MainPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.MainPanel_Paint);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.btnMarkEvaluation);
            this.groupBox1.Controls.Add(this.btnViewEvaluations);
            this.groupBox1.Location = new System.Drawing.Point(321, 234);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(556, 399);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // btnMarkEvaluation
            // 
            this.btnMarkEvaluation.BorderRadius = 5;
            this.btnMarkEvaluation.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMarkEvaluation.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnMarkEvaluation.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnMarkEvaluation.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnMarkEvaluation.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnMarkEvaluation.FillColor = System.Drawing.Color.Navy;
            this.btnMarkEvaluation.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnMarkEvaluation.ForeColor = System.Drawing.Color.White;
            this.btnMarkEvaluation.HoverState.FillColor = System.Drawing.Color.RoyalBlue;
            this.btnMarkEvaluation.Location = new System.Drawing.Point(297, 62);
            this.btnMarkEvaluation.Name = "btnMarkEvaluation";
            this.btnMarkEvaluation.Size = new System.Drawing.Size(181, 81);
            this.btnMarkEvaluation.TabIndex = 1;
            this.btnMarkEvaluation.Text = "Mark Evaluations";
            this.btnMarkEvaluation.Click += new System.EventHandler(this.btnMarkEvaluation_Click_1);
            // 
            // btnViewEvaluations
            // 
            this.btnViewEvaluations.BorderRadius = 5;
            this.btnViewEvaluations.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnViewEvaluations.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btnViewEvaluations.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btnViewEvaluations.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btnViewEvaluations.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btnViewEvaluations.FillColor = System.Drawing.Color.Navy;
            this.btnViewEvaluations.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.btnViewEvaluations.ForeColor = System.Drawing.Color.White;
            this.btnViewEvaluations.HoverState.FillColor = System.Drawing.Color.RoyalBlue;
            this.btnViewEvaluations.Location = new System.Drawing.Point(75, 62);
            this.btnViewEvaluations.Name = "btnViewEvaluations";
            this.btnViewEvaluations.Size = new System.Drawing.Size(181, 81);
            this.btnViewEvaluations.TabIndex = 0;
            this.btnViewEvaluations.Text = "View Evaluations";
            this.btnViewEvaluations.Click += new System.EventHandler(this.btnViewEvaluations_Click);
            // 
            // guna2Panel1
            // 
            this.guna2Panel1.BackColor = System.Drawing.Color.DodgerBlue;
            this.guna2Panel1.Controls.Add(this.label1);
            this.guna2Panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.guna2Panel1.Location = new System.Drawing.Point(0, 0);
            this.guna2Panel1.Name = "guna2Panel1";
            this.guna2Panel1.Size = new System.Drawing.Size(1141, 100);
            this.guna2Panel1.TabIndex = 0;
            this.guna2Panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.guna2Panel1_Paint);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(396, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(423, 67);
            this.label1.TabIndex = 0;
            this.label1.Text = "Evaluation Menu";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // EvaluationMenuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.MainPanel);
            this.Name = "EvaluationMenuForm";
            this.Size = new System.Drawing.Size(1141, 872);
            this.MainPanel.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.guna2Panel1.ResumeLayout(false);
            this.guna2Panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2GradientPanel MainPanel;
        private System.Windows.Forms.GroupBox groupBox1;
        private Guna.UI2.WinForms.Guna2Button btnViewEvaluations;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2Button btnMarkEvaluation;
    }
}
