﻿using DBProject.DatabaseQueries;
using MidProject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject.UserForms.Evaluation
{
    public partial class MarkEvaluationForm : UserControl
    {
        public string EmpID = "-1";
        public string MetricID, MetricName, TotalScore, Weightage;
        public MarkEvaluationForm()
        {
            InitializeComponent();
            PopulateComboBox();
        }

        public void PopulateComboBox()
        {
            SqlCommand cmd = PerformanceMatricsQueries.GetAllJobs();
            cmd.ExecuteNonQuery();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                cmbDept.Items.Add(reader.GetString(0));
            }
            reader.Close();
        }

        public void PopulateEmployeeGrid()
        {
            string deptName = cmbDept.Text;
            SqlCommand cmd = EmployeeQueries.GetEmployeeByDepartment(deptName);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            gridEmp.DataSource = dt;
        }

        public void PopulatePerformanceMatricTable()
        {
            string jobName = cmbDept.Text;
            SqlCommand cmd = PerformanceMatricsQueries.GetJobByName(jobName);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            gridPerf.DataSource = dt;

        }

        public void PopulateScoreTable()
        {
            SqlCommand cmd = EmployeeQueries.GetScoreOfEmployee(EmpID);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            gridScore.DataSource = dt;
        }

        public int PresentCount(int empID)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select count(*) From Attendance where AttendanceStatus = 5 and EmployeeID = @empID", con);
            cmd.Parameters.AddWithValue("@empID", empID);
            int count = cmd.ExecuteNonQuery();
            return count;
        }

        public int AbsentCount(int EmpID)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select count(*) From Attendance where AttendanceStatus = 6 and EmployeeID = @empID", con);
            cmd.Parameters.AddWithValue("@empID", EmpID);
            int count = cmd.ExecuteNonQuery();
            return count;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string obtainedMarks = txtObtainedMarks.Text;
            Double Score = ((Convert.ToDouble(obtainedMarks) / Convert.ToDouble(TotalScore)) * Convert.ToDouble(Weightage));
            DateTime date = DateTime.Now;
            int count = EmployeeQueries.CheckTheMetricIDInEmp_Performance(EmpID, MetricID);
            if(count == 0)
            {
                EmployeeQueries.AddInEmp_Performance(EmpID, MetricID, Score.ToString(), date);
                PopulateScoreTable();
            }
            else
            {
                MessageBox.Show("Score already inserted against this Metric","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            txtObtainedMarks.Text = string.Empty;
            
        }

        private void cmbDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name = cmbDept.Text;
            lblDept.Text = name;
            PopulateEmployeeGrid();
            PopulatePerformanceMatricTable();
        }

        private void gridEmp_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gridEmp.SelectedRows.Count > 0)
            {
                EmpID = gridEmp.SelectedRows[0].Cells[0].Value.ToString();
                if (EmpID != "-1")
                {
                     PopulateScoreTable();
                    
                }

            }
        }

        private void gridPerf_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (gridEmp.SelectedRows.Count > 0)
            {
                MetricID = gridPerf.SelectedRows[0].Cells[0].Value.ToString();
                MetricName = gridPerf.SelectedRows[0].Cells[1].Value.ToString();
                TotalScore = gridPerf.SelectedRows[0].Cells[2].Value.ToString();
                Weightage = gridPerf.SelectedRows[0].Cells[3].Value.ToString();
                lblScore.Text = TotalScore;
                lblMetricName.Text = MetricName;

            }
        }
    }
}
