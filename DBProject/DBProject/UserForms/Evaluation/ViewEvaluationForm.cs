﻿using DBProject.DatabaseQueries;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject.UserForms.Evaluation
{
    public partial class ViewEvaluationForm : UserControl
    {
        public ViewEvaluationForm()
        {
            InitializeComponent();
            PopulateTable();
            PopulateComboBox();
            btnBack.Visible = false;
        }

        public void PopulateTable()
        {
            SqlCommand cmd = EmployeeQueries.GetScoreOfAllEmployees();
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            gridEvaluation.DataSource = dt;
        }

        private void gridEvaluation_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public void PopulateComboBox()
        {
            SqlCommand cmd = PerformanceMatricsQueries.GetAllJobs();
            cmd.ExecuteNonQuery();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                cmbDept.Items.Add(reader.GetString(0));
            }
            reader.Close();
        }

        public void ViewSpecificJobs()
        {
            string deptName = cmbDept.Text;
            SqlCommand cmd = EmployeeQueries.GetScoreOfEmployeesbyJob(deptName);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            gridEvaluation.DataSource = dt;
        }

        public void GetEmployeeByName()
        {
            string name = txtsearch.Text;
            if(name != string.Empty)
            {
                SqlCommand cmd = EmployeeQueries.GetScoreOfEmployeesbyName(name);
                cmd.ExecuteNonQuery();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                gridEvaluation.DataSource = dt;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            GetEmployeeByName();
            btnBack.Visible = true;
        }

        private void cmbDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewSpecificJobs();
            btnBack.Visible = true;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            PopulateTable();
            btnBack.Visible = false;
            txtsearch.Text = string.Empty;
        }
    }
}
