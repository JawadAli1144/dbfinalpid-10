﻿using DBProject.UserForms.Evaluation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject.UserForms
{
    public partial class EvaluationMenuForm : UserControl
    {
        public EvaluationMenuForm()
        {
            InitializeComponent();
        }

        private void addPanel(UserControl usercontrol)
        {
            usercontrol.Dock = DockStyle.Fill;
            MainPanel.Controls.Clear();
            MainPanel.Controls.Add(usercontrol);
            usercontrol.BringToFront();

        }

        private void btnMarkEvaluation_Click(object sender, EventArgs e)
        {
            MarkEvaluationForm markev = new MarkEvaluationForm();
            addPanel(markev);
        }

        private void btnViewEvaluations_Click(object sender, EventArgs e)
        {
            ViewEvaluationForm viewEv = new ViewEvaluationForm();
            addPanel(viewEv);
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnMarkEvaluation_Click_1(object sender, EventArgs e)
        {
            MarkEvaluationForm mrk = new MarkEvaluationForm();
            addPanel(mrk);
        }

        private void MainPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
