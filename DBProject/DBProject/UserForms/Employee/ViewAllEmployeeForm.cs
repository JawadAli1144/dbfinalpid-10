﻿using DBProject.DatabaseQueries;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject.UserForms.Employee
{
    public partial class ViewAllEmployeeForm : UserControl
    {
        public ViewAllEmployeeForm()
        {
            InitializeComponent();
            PopulateTable();
            PopulateComboBox();
            btnBack.Visible = false;
        }

        public void PopulateComboBox()
        {
            SqlCommand cmd = PerformanceMatricsQueries.GetAllJobs();
            cmd.ExecuteNonQuery();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                cmbJobs.Items.Add(reader.GetString(0));
            }
            reader.Close();
        }

        public void PopulateTable()
        {
            SqlCommand cmd = EmployeeQueries.GetAllEmployees();
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string name = txtSearch.Text;
            SqlCommand cmd = EmployeeQueries.GetEmployeeByName(name);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            btnBack.Visible = true;

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            PopulateTable();
            txtSearch.Text = string.Empty;
            btnBack.Visible = false;
        }

        public void ViewEmployessByJob()
        {
            string JobName = cmbJobs.Text;
            SqlCommand cmd = EmployeeQueries.GetAllEmployeesByJob(JobName);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void cmbJobs_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewEmployessByJob();
            btnBack.Visible = true;
        }

        private void MainPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {

        }

        private void guna2Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void addPanel(UserControl usercontrol)
        {
            usercontrol.Dock = DockStyle.Fill;
            MainPanel.Controls.Clear();
            MainPanel.Controls.Add(usercontrol);
            usercontrol.BringToFront();

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddEmployeeMenu addemp = new AddEmployeeMenu();
            addPanel(addemp);           

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                string id = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                EmployeeQueries.DeleteEmployee(id);
                MessageBox.Show("Employee has been deleted successfully");
                
                

            }
            else
            {
                MessageBox.Show("Select Row First", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
    }
}
