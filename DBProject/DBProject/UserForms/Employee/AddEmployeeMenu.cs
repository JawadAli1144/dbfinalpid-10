﻿using DBProject.DatabaseQueries;
using MidProject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace DBProject.UserForms.Employee
{
    public partial class AddEmployeeMenu : UserControl
    {
        public AddEmployeeMenu()
        {
            InitializeComponent();
            PopulateJobsComboBox();
            PopulateManagerComboBox();
        }

        private void addPanel(UserControl usercontrol)
        {
            usercontrol.Dock = DockStyle.Fill;
            MainPanel.Controls.Clear();
            MainPanel.Controls.Add(usercontrol);
            usercontrol.BringToFront();

        }

        public void PopulateJobsComboBox()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select JobTitle From Jobs where Status = 1", con);
            cmd.ExecuteNonQuery();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                cmbJob.Items.Add(reader.GetString(0));
            }
            reader.Close();
        }

        public void PopulateManagerComboBox()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select CONCAT(Per_FName,' ',Per_LName) From Person where Per_status = 1", con);
            cmd.ExecuteNonQuery();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                cmbManager.Items.Add(reader.GetString(0));
            }
            reader.Close();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ViewAllEmployeeForm viewEmp = new ViewAllEmployeeForm();
            addPanel(viewEmp);
        }

        public void ClearTexts()
        {
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtPhone.Text = string.Empty;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string FirstName = txtFirstName.Text;
            string LastName = txtLastName.Text;
            string Email = txtEmail.Text;
            string Phone = txtPhone.Text;
            string GenderID = EmployeeQueries.GetGenderID(cmbGender.Text).ToString();
            string JobID = EmployeeQueries.GetJobID(cmbJob.Text).ToString();
            string ManagerID = null;
            if (cmbManager.Text != string.Empty)
            {
                string[] parts = (cmbManager.Text).Split(new[] { ' ' }, 2);
                string firstname = parts[0];
                string lastname = parts[1];
                ManagerID = EmployeeQueries.GetManagerID(firstname, lastname).ToString();
            }

            try
            {
                EmployeeQueries.InsertIntoPersonTable(FirstName, LastName, Email, Phone, "1", GenderID);
                int maxID = EmployeeQueries.SelectMaxFromPersonTable();
                EmployeeQueries.InsertIntoEmployeeTable(maxID.ToString(), JobID, DateTime.Now, ManagerID);

                MessageBox.Show("Employee Added Successfully");
            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("Cannot assign more than 5 employees to this job."))
                {
                    MessageBox.Show("Cannot assign more than 5 employees to this job.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                }
                else
                {
                    MessageBox.Show("An error occurred while adding the employee", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                }
            }
            ClearTexts();

            

        }

        private void cmdJob_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbManager_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
