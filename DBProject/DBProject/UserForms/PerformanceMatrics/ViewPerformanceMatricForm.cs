﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DBProject.DatabaseQueries;
using DBProject.UserForms.Job;
using MidProject;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace DBProject.UserForms.PerformanceMatrics
{
    public partial class ViewPerformanceMatricForm : UserControl
    {
        bool checkAdd = false;
        public ViewPerformanceMatricForm()
        {
            InitializeComponent();
            PopulateTable();
            PopulateComboBox();
            lblHeader.Text = "Performance Metrics";
            checkAdd = false;
        }

        public void PopulateTable()
        {
            SqlCommand cmd = PerformanceMatricsQueries.ViewAllPerfomanceMatric();
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        public void PopulateComboBox()
        {
            SqlCommand cmd = PerformanceMatricsQueries.GetAllJobs();
            cmd.ExecuteNonQuery();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                cmbJobs.Items.Add(reader.GetString(0));
            }
            reader.Close();
        }

        public void ViewSpecificJobs()
        {
            string JobName = cmbJobs.Text;
           SqlCommand cmd = PerformanceMatricsQueries.ViewAllPerfomanceMatricOfSpecificJob(JobName);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void addPanel(UserControl usercontrol)
        {
            usercontrol.Dock = DockStyle.Fill;
            MainPanel.Controls.Clear();
            MainPanel.Controls.Add(usercontrol);
            usercontrol.BringToFront();

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(checkAdd == true)
            {
              string jobId = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                AddPerformanceMetricForm addMetric = new AddPerformanceMetricForm(jobId);
                addPanel(addMetric);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {

        }

        private void cmbDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewSpecificJobs();
        }

        private void btnViewAll_Click(object sender, EventArgs e)
        {
            PopulateTable();
           // cmbDept.Text = "";
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            // add performance metric
            checkAdd = true;
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * From Jobs", con);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            lblHeader.Text = "Select Job";
            cmbJobs.Visible = false;
            btnViewAll.Visible = false;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

        }
    }
}
