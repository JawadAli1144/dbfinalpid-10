﻿using DBProject.DatabaseQueries;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject.UserForms.PerformanceMatrics
{
    public partial class AddPerformanceMetricForm : UserControl
    {
        string jobID;
        public AddPerformanceMetricForm(string jobID)
        {
            InitializeComponent();
            this.jobID = jobID;
            lbljobTitle.Text = JobQueries.GetJobName(jobID).ToString();
            
        }

        private void addPanel(UserControl usercontrol)
        {
            usercontrol.Dock = DockStyle.Fill;
            MainPanel.Controls.Clear();
            MainPanel.Controls.Add(usercontrol);
            usercontrol.BringToFront();

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ViewPerformanceMatricForm viewPerf = new ViewPerformanceMatricForm();
            addPanel(viewPerf);
        }

        private void lbljobTitle_Click(object sender, EventArgs e)
        {

        }

        public void ClearTexts()
        {
            txtMetricName.Text = string.Empty;
            txtTotalScore.Text = string.Empty;
            txtTotalWeightage.Text = string.Empty;
            txtDesc.Text = string.Empty;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string MetricName = txtMetricName.Text;
            string TotalMarks = txtTotalScore.Text;
            string Weightage = txtTotalWeightage.Text;
            string description = txtDesc.Text;


            try
            {
                PerformanceMatricsQueries.InsertIntoPerformanceMetricTable(jobID, MetricName, Weightage, TotalMarks, description);
                MessageBox.Show("Performance Metric Added Successfully");
            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("Cannot add more than 5 metrics to a job."))
                {
                    MessageBox.Show(" Cannot add more then 5 metrics to this job", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("An error occurred while adding the employee: ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    
                }
            }

            
            ClearTexts();
        }
    }
}
