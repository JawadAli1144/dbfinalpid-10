﻿using DBProject.DatabaseQueries;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject.UserForms.Job
{
    public partial class ViewJobForm : UserControl
    {
        public ViewJobForm()
        {
            InitializeComponent();
            PooulateTable();
        }

        public void PooulateTable()
        {
            SqlCommand cmd = JobQueries.ViewJobs();
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void addPanel(UserControl usercontrol)
        {
            usercontrol.Dock = DockStyle.Fill;
            MainPanel.Controls.Clear();
            MainPanel.Controls.Add(usercontrol);
            usercontrol.BringToFront();

        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddJobForm addJob = new AddJobForm();
            addPanel(addJob);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            // delete job

            if (dataGridView1.SelectedRows.Count > 0)
            {
                string id = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                JobQueries.DeleteJob(id);
                MessageBox.Show("Job Have been deleted successfully");

            }
            else
            {
                MessageBox.Show("Select Row First", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
    }
}
