﻿using DBProject.DatabaseQueries;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject.UserForms.Job
{
    public partial class AddJobForm : UserControl
    {
        public AddJobForm()
        {
            InitializeComponent();
        }

        private void addPanel(UserControl usercontrol)
        {
            usercontrol.Dock = DockStyle.Fill;
            MainPanel.Controls.Clear();
            MainPanel.Controls.Add(usercontrol);
            usercontrol.BringToFront();

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ViewJobForm viewjob = new ViewJobForm();
            addPanel(viewjob);
        }

        public void ClearTexts()
        {
            txtJobName.Text = string.Empty;
            txtSalary.Text = string.Empty;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            string JobName = txtJobName.Text;
            string salary = txtSalary.Text;
            JobQueries.AddIntoJob(JobName, salary, "1");
            MessageBox.Show("Job Added Successfully");
        }
    }
}
