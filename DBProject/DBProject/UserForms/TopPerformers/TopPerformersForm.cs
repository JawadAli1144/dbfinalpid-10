﻿using DBProject.DatabaseQueries;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject.UserForms.TopPerformers
{
    public partial class TopPerformersForm : UserControl
    {
        public TopPerformersForm()
        {
            InitializeComponent();
            PopulateTable();
            PopulateComboBox();
        }

        public void PopulateTable()
        {
            SqlCommand cmd = TopPerformerQueries.InsertIntoTopPerformerTable();
            cmd.ExecuteNonQuery();
            SqlCommand cmd2 = TopPerformerQueries.ViewTopPerformers();
            cmd2.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd2);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        public void PopulateComboBox()
        {
            SqlCommand cmd = PerformanceMatricsQueries.GetAllJobs();
            cmd.ExecuteNonQuery();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                cmbDept.Items.Add(reader.GetString(0));
            }
            reader.Close();
        }

        public void PopulateTablewithSpecificDept()
        {
            string deptName = cmbDept.Text;
            SqlCommand cmd = TopPerformerQueries.ViewEmployeeOfSpecificDept(deptName);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cmbDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateTablewithSpecificDept();
        }

        private void btnViewAll_Click(object sender, EventArgs e)
        {
            PopulateTable();
        }
    }
}
