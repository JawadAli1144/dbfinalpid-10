﻿using DBProject.DatabaseQueries;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject.UserForms.Attendance
{
    public partial class ViewAttendanceRecord : UserControl
    {
        string EmpID = "-1";
        public ViewAttendanceRecord()
        {
            InitializeComponent();
            PopulateTable();
            btnBack.Visible = false;
        }

        public void PopulateTable()
        {
            SqlCommand cmd = AttendanceQueries.ViewEmployees();
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        private void addPanel(UserControl usercontrol)
        {
            usercontrol.Dock = DockStyle.Fill;
            MainPanel.Controls.Clear();
            MainPanel.Controls.Add(usercontrol);
            usercontrol.BringToFront();
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                EmpID = dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
                if (EmpID != "-1")
                {
                    ViewAttendance viewatt = new ViewAttendance(EmpID);
                    addPanel(viewatt);
                }

            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string name = txtSearch.Text;
            SqlCommand cmd = AttendanceQueries.ViewEmployees(name);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
            btnBack.Visible = true;

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            PopulateTable();
            btnBack.Visible = false;
        }

        private void MainPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {

        }

        private void guna2Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
