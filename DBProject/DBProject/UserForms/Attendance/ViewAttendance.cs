﻿using DBProject.DatabaseQueries;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBProject.UserForms.Attendance
{
    public partial class ViewAttendance : UserControl
    {
        public string EmployeeID;
        public ViewAttendance(string EmployeeID)
        {
            InitializeComponent();
            this.EmployeeID = EmployeeID;
            PopulateTable();
        }
        
        public void PopulateTable()
        {
            SqlCommand cmd = AttendanceQueries.ViewAttendance(EmployeeID);
            cmd.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void addPanel(UserControl usercontrol)
        {
            usercontrol.Dock = DockStyle.Fill;
            MainPanel.Controls.Clear();
            MainPanel.Controls.Add(usercontrol);
            usercontrol.BringToFront();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            ViewAttendanceRecord viewatt = new ViewAttendanceRecord();
            addPanel(viewatt);
        }
    }
}
